﻿using BotDetect.Web.Mvc;
using Facebook;
using Model.BusinessModel;
using Model.DataModel;
using Model.ViewModel;
using OnlineShop.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OnlineShop.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        //Login Facebook
        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("FacebookCallBack");
                return uriBuilder.Uri;
            }
        }
        public ActionResult FacebookCallBack(string code)
        {
            var fbClient = new FacebookClient();
            dynamic result = fbClient.Post("oauth/access_token", new
            {
                client_id = ConfigurationManager.AppSettings["FbAppID"],
                client_secret = ConfigurationManager.AppSettings["FbAppSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                code = code
            });

            //lay token
            var accessToken = result.access_token;

            if (!string.IsNullOrEmpty(accessToken))
            {
                //set token
                fbClient.AccessToken = accessToken;
                //login

                dynamic me = fbClient.Get("me?fields=first_name,middle_name,last_name,id,email");
                string email = me.email;
                string userName = me.email;
                string firstName = me.first_name;
                string lastName = me.last_name;
                string middleName = me.middle_name;

                User user = new User();
                user.Email = email;
                user.UserName = email;
                user.Name = firstName + " " + middleName + " " + lastName;
                var kq = new UserDAO().InsertForFacebook(user);
                //Set cookie
                FormsAuthentication.SetAuthCookie(userName, true);
            }
            return Redirect("/");
        }
        public ActionResult LoginFacebook()
        {
            var fbClient = new FacebookClient();
            var loginUrl = fbClient.GetLoginUrl(new
            {
                client_id = ConfigurationManager.AppSettings["FbAppID"],
                client_secret = ConfigurationManager.AppSettings["FbAppSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                redirect_type = "code",
                scope = "email",
            });
            return Redirect(loginUrl.AbsoluteUri);
        }
        //Login Facebook
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        [CaptchaValidation("CaptchaCode", "myCaptcha", "Incorrect CAPTCHA code!")]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                UserDAO dao = new UserDAO();
                dao.Insert(model);
                ViewBag.success = "thanh cong";

                //Send Email
                string content = System.IO.File.ReadAllText(Server.MapPath("~/Assets/Client/templateEmail/Email.html"));
                content = content.Replace("{{CustomName}}", "Le Minh Triet");
                EmailHelper EmailHelper = new EmailHelper();
                EmailHelper.SendEmail("minhtriet193@gmail.com", "test mail", content);
            }else
            {
                ModelState.AddModelError("", "Loi dang ky");
            }
            return View();
        }
    }
}
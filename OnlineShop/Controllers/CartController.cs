﻿using Common;
using Model.DataModel;
using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShop.Controllers
{
    public class CartController : Controller
    {
        OnlineShopDbContext db = null;
        public CartController()
        {
            db = new OnlineShopDbContext();
        }
        // GET: Cart
        public ActionResult Index()
        {
            var model = (List<CartItem>)Session[CommonConstants.CURRENT_CART];
            return View(model);
        }

        public ActionResult AddCart(long productID, int quatity)
        {
            var cartSession = (List<CartItem>)Session[CommonConstants.CURRENT_CART];
            if(cartSession != null)
            {
                if(cartSession.Exists(x=>x.Product.ID == productID))
                {
                    //kiem tra neu co thi + them
                    foreach (var item in cartSession)
                    {
                        if (item.Product.ID == productID)
                        {
                            item.Quantity += quatity;
                        }
                    }

                    Session[CommonConstants.CURRENT_CART] = cartSession;
                }
                else
                {
                    CartItem item = new CartItem();
                    item.Product.ID = productID;
                    item.Product.Price = db.Product.SingleOrDefault(x => x.ID == productID).Price;
                    item.Quantity = quatity;
                    cartSession.Add(item);

                    Session[CommonConstants.CURRENT_CART] = cartSession;
                }
            }
            else
            {
                var listCart = new List<CartItem>();
                CartItem item = new CartItem();
                item.Product.ID = productID;
                item.Product.Price = db.Product.SingleOrDefault(x => x.ID == productID).Price;
                item.Quantity = quatity;
                listCart.Add(item);

                Session[CommonConstants.CURRENT_CART] = listCart;
            }
            return RedirectToAction("Index");
        }
    }
}
﻿var product = {
    init: function () {
        product.registerEvent();
    },
    registerEvent : function(){
        $("#browser").on("click", function () {
            var finder = new CKFinder();
            finder.selectActionFunction = function (fileUrl) {
                $("#Image").val(fileUrl);
            }
            finder.popup();
        });
    }
}
product.init();
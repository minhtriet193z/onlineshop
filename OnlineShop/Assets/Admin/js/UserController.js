﻿var user =  {
    init: function(){
        user.loadProvince();
    },
    registerEvent: function(){

    },
    loadProvince: function () {
        $.ajax({
            url: "getProvince",
            type: "Get",
            success: function (res) {
                if (res.status == true) {
                    var root = $("#Province");
                    var province = "";
                    $.each(res.data, function (index, Item) {
                        province += "<option value='" + Item.ID + "'>" + Item.Value + "</option>"
                    });
                    root.append(province)
                }  
            }
        });
    }
}
user.init();


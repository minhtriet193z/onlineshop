﻿var admin = {
    init: function () {
        admin.registerEvent();
    },
    registerEvent: function () {
        $("#txtSearch").autocomplete({
            minLength: 0,
            source: function (request, response) {
                $.ajax({
                    url: "/Admin/User/Search",
                    dataType: "json",
                    data: {
                        stringSearch: request.term
                    },
                    success: function (res) {
                        response(res.data);
                    }
                });
            },
            focus: function (event, ui) {
                $("#txtSearch").val(ui.item.Name);
                return false;
            },
            select: function (event, ui) {
                $("#txtSearch").val(ui.item.Name);
                return false;
            }
        })
    .autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
          .append("<div>" + item.Name + "</div>")
          .appendTo(ul);
    };
    }
}
admin.init();
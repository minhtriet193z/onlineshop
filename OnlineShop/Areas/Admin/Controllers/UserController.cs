﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model.DataModel;
using PagedList;
using Model.BusinessModel;
using System.Web.Routing;
using System.Threading;
using System.Globalization;
using Common;
using System.Xml.Linq;

namespace OnlineShop.Areas.Admin.Controllers
{
    public class UserController : Controller
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if(Session[CommonConstants.CURRENT_CULTURE] != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Session[CommonConstants.CURRENT_CULTURE].ToString());
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session[CommonConstants.CURRENT_CULTURE].ToString());
            }
            else
            {
                Session[CommonConstants.CURRENT_CULTURE] = "vi";
                Thread.CurrentThread.CurrentCulture = new CultureInfo("vi");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("vi");
            }
        }

        public ActionResult ChangeCulture(string ddlCulture, string returnUrl)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo(ddlCulture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(ddlCulture);

            Session[CommonConstants.CURRENT_CULTURE] = ddlCulture;
            return Redirect(returnUrl);
        }

        private OnlineShopDbContext db = new OnlineShopDbContext();

        // GET: Admin/User
        public ActionResult Index(string searchString = "", int page = 1, int pageSize = 2)
        {
            var model = new UserDAO().getAllPaging(searchString, page, pageSize);
            ViewBag.searchString = searchString;
            return View(model);
        }

        // GET: Admin/User/Details/5
        [OutputCache(CacheProfile = "Cache1HourForUser")]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User User = db.User.Find(id);
            if (User == null)
            {
                return HttpNotFound();
            }
            return View(User);
        }

        // GET: Admin/User/Create
        public ActionResult Create()
        {
            UserGroupDAO dao = new UserGroupDAO();
            ViewBag.GroupID = new SelectList(dao.getList(), "ID", "Name");
            return View();
        }

        // POST: Admin/User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,UserName,Password,Phone,Email,GroupID,CreateDate,CreateBy,ModifiedDate,ModifiedBy,MetaKeyWords,MetaDescriptions")] User User)
        {
            if (ModelState.IsValid)
            {
                User.Password = Encryptor.MD5Hash(User.Password);
                db.User.Add(User);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(User);
        }

        // GET: Admin/User/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User User = db.User.Find(id);
            if (User == null)
            {
                return HttpNotFound();
            }
            return View(User);
        }

        // POST: Admin/User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,UserName,Password,Phone,Email,GroupID,CreateDate,CreateBy,ModifiedDate,ModifiedBy,MetaKeyWords,MetaDescriptions")] User User)
        {
            if (ModelState.IsValid)
            {
                db.Entry(User).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(User);
        }

        // GET: Admin/User/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User User = db.User.Find(id);
            if (User == null)
            {
                return HttpNotFound();
            }
            return View(User);
        }

        // POST: Admin/User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            User User = db.User.Find(id);
            db.User.Remove(User);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult Search(string stringSearch)
        {
            var data = db.User.Where(x => x.Name.Contains(stringSearch)).OrderBy(x => x.ID).ToList();
            return Json(new
            {
                data = data,
                status = true
            },JsonRequestBehavior.AllowGet);
        }

        public JsonResult getProvince()
        {
            XDocument xdoc = XDocument.Load(Server.MapPath("~/Assets/Admin/data/Provinces_Data.xml"));
            var elements = xdoc.Element("Root").Elements("Item").Where(x => x.Attribute("type").Value == "province");
            List<Province> list = new List<Province>();
            foreach (var item in elements)
            {
                Province v = new Province();
                v.ID = item.Attribute("id").Value;
                v.Value = item.Attribute("value").Value;
                list.Add(v);
            }
            return Json(new { data = list, status = true },JsonRequestBehavior.AllowGet);
        } 
    }
}

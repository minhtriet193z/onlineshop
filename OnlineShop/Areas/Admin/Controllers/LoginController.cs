﻿using Model.BusinessModel;
using OnlineShop.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OnlineShop.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        // GET: Admin/Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                CustomMemberShipProvider provider = new CustomMemberShipProvider();
                if (provider.ValidateUser(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, true);
                    return RedirectToAction("Index", "Home");
                }
                /*
                LoginDAO dao = new LoginDAO();
                bool res = dao.Login(model.UserName, model.Password);
                if(res == true)
                {
                    //set Session
                    UserSessionHelper.SetUserSession(new UserSession() { UserName = model.UserName });
                    return RedirectToAction("Index", "Home");
                }
                */
                else
                {
                    ModelState.AddModelError("", "Loi dang nhap");
                    return View();
                }
            }
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");
        }
    }
}
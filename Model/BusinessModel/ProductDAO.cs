﻿using Model.DataModel;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.BusinessModel
{
    public class ProductDAO
    {
        OnlineShopDbContext db = null;
        public ProductDAO()
        {
            db = new OnlineShopDbContext();
        }
        public IEnumerable<Product> getAllPaging(int pageIndex, int pageSize)
        {
            return db.Product.OrderBy(x=>x.ID).ToPagedList(pageIndex,pageSize);
        }
    }
}

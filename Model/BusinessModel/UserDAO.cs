﻿using Model.DataModel;
using Model.ViewModel;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.BusinessModel
{
    public class UserDAO
    {
        OnlineShopDbContext db = null;
        public UserDAO()
        {
            db = new OnlineShopDbContext();
        }

        public IEnumerable<User> getAllPaging(string searchString, int page, int pageSize)
        {
            if (!String.IsNullOrEmpty(searchString))
                return db.User.Where(x => x.Name.Contains(searchString) || x.UserName.Contains(searchString)).OrderBy(x => x.ID).ToPagedList(page, pageSize);
            return db.User.OrderBy(x => x.ID).ToPagedList(page, pageSize);
        }

        public bool Insert(RegisterModel model) {
            var res = true;
            User u = new User();
            u.UserName = model.UserName;
            u.Password = model.Password;
            db.User.Add(u);
            db.SaveChanges();
            return res;
        }
        public long InsertForFacebook(User user)
        {
            db.User.Add(user);
            db.SaveChanges();
            return 1;
        }
    }
}

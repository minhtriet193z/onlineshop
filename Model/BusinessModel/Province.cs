﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.BusinessModel
{
    public class Province
    {
        public string ID { get; set; }

        public string Value { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Model.BusinessModel
{
    public class UserSessionHelper
    {
        public static void SetUserSession(UserSession user)
        {
            HttpContext.Current.Session["UserSession"] = user.UserName;
        }

        public static UserSession GetUserSession()
        {
            var user = HttpContext.Current.Session["UserSession"];
            if(user == null)
            {
                return null;
            }else
            {
                return user as UserSession;
            }
        }
    }
}

﻿using Common;
using Model.DataModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.BusinessModel
{
    
    public class LoginDAO
    {
        OnlineShopDbContext db = null;
        public LoginDAO(){
            db = new OnlineShopDbContext();
        }

        public bool Login(string userName, string passWord)
        {
            int res = 0;
            passWord = Encryptor.MD5Hash(passWord);

            Object[] sqlParameter = {
                new SqlParameter("@UserName", userName),
                new SqlParameter("@Password", passWord)
            };
            res = db.Database.SqlQuery<int>("Sp_Account_Login @UserName, @Password", sqlParameter).SingleOrDefault();
            if (res == 1)
                return true;
            return false;
        }
    }
}

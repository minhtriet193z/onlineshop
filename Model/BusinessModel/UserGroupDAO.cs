﻿using Model.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.BusinessModel
{
    public class UserGroupDAO
    {
        OnlineShopDbContext db = null;
        public UserGroupDAO()
        {
            db = new OnlineShopDbContext();
        }
        public List<UserGroup> getList()
        {
            return db.UserGroup.ToList();
        }
    }
}
